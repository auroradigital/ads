$(document).ready(function() {
  $(".home").click(function() {
      var offset = $('#home').offset();
    $("html,body").animate({
        scrollTop: offset.top,
        scrollLeft: offset.left
    });
  });

  $(".whoweare").click(function() {
      var offset = $('#who-we-are').offset();
    $("html,body").animate({
        scrollTop: offset.top,
        scrollLeft: offset.left
    });
  });

  $(".whatwedo").click(function() {
      var offset = $('#bg-what-we-do').offset();
    $("html,body").animate({
        scrollTop: offset.top,
        scrollLeft: offset.left
    });
  });

  $(".thework").click(function() {
      var offset = $('#our-work').offset();
    $("html,body").animate({
        scrollTop: offset.top,
        scrollLeft: offset.left
    });
  });

  $(".makecontact").click(function() {
      var offset = $('#bg-contact').offset();
    $("html,body").animate({
        scrollTop: offset.top,
        scrollLeft: offset.left
    });
  });

  // Mobile Nav (Make disappear onclick)
  $('.nav a').on('click', function(){ 
      if($('.navbar-toggle').css('display') !='none'){
          $(".navbar-toggle").trigger( "click" );
      }
  });

  $('.accordion').collapse();

  $('#navbar').scrollspy();

  $('.thumbnail').tooltip();

  // Form Submission
  $('form').on('submit', function(e) {
    e.preventDefault();

    // Spam Check
    var $spm = $('#real').val();
    var $ctnContact = $('.contact-success');

    // Destroy previous Alert
    $ctnContact.empty();

    if ( $spm == 9 ) {
      // If Spam Check is good then re-enable normal border.
      $('#real').css('border-color', 'rgba(255, 255, 255, 0.2)');

          var $btn = $(this).find('button');

          $btn.attr('disabled', true);
          $btn.val('Submitting...');

          $.ajax({
              type: 'POST',
              data: $('form').serialize(),
              url: $(this).attr('action'),
              success: function(msg) {
                  //var targetOffset = $('.most_recent').offset().top;

                  // Insert post
                  $ctnContact.attr('style', 'display: block; margin-bottom: 12px;');
                  $(msg).appendTo('#bg-contact .contact-success');

                  // Scroll to top of posts
                  //$("body, html").animate({scrollTop: targetOffset}, 600);

                  // Re-enable submit button
                  $btn.removeAttr('disabled');

                  // Update button text
                  $btn.val('Send Contact Info');

                  // Clear old post message
                  $('form input').val('');
                  $('form textarea').val('');

                  // Clear Spam Warning Class
                  $('.contact-success').removeClass('has-spam');
              }
          });
      } else {
        $ctnContact.attr('style', 'display: block; margin-bottom: 12px;');
        $ctnContact.append('<div class="bg-danger pad-large">Incorrect Spam Check! Please enter the correct number below.</div>');
        $ctnContact.addClass('has-spam');
        $('#real').css('border-color', '#ff0000');
      }
    });
});