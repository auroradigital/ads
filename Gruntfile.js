module.exports = function(grunt){

	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // Project configuration.
		concat: {
			options: {
				separator: ';'
			},
			basic: {
				src: ['src/js/*.js'],
				dest: 'build/js/base.js',
			}
		},
		concat_css: {
			all: {
				src: ['src/css/*.css'],
				dest: 'build/css/base.css'
			}
		},
		less: {
			options: {
		        paths: ['src/less']
		    },
		    src: {
		        expand: true,
		        cwd:    'src/less',
		        src:    '*.less',
		        dest: 	'src/css',
		        ext:    '.css'
		    }
		},
        htmlhint: {
		    build: {
		        options: {
		            'tag-pair': true,
		            'tagname-lowercase': true,
		            'attr-lowercase': true,
		            'attr-value-double-quotes': true,
		            'doctype-first': true,
		            'spec-char-escape': true,
		            'id-unique': true,
		            'head-script-disabled': true,
		            'style-disabled': true
		        },
		        src: ['./*.html']
		    }
		},
		// Minify CSS
		cssmin: {
			target: {
				files: [{
					expand: true,
					cwd: 'build/css',
					src: ['base.css'],
					dest: 'build/css',
					ext: '.min.css'
				}]
			}
		},
		// Minify JS
		uglify: {
			dist: {
				files: {
					'build/js/base.min.js': ['<%= concat.basic.dest %>']
				}
			}
		},
		jasmine: {
			pivotal: {
				src: 'js/*.js',
				options: {
					specs: 'spec/*Spec.js',
					helpers: 'spec/*Helper.js'
				}
			}
		},
		// Check the JS for errors
		jshint: {
			files: ['Gruntfile.js', 'src/js/*.js', '!js/bootstrap.js'],
			options: {
				// options here to override JSHint defaults
				globals: {
					jQuery: true,
					console: true,
					module: true,
					document: true
				}
			}
		},
		// Check the CSS for errors.
		csslint: {
			strict: {
				options: {
					import: 2
				},
				src: ['src/css/*.css']
			},
			lax: {
				options: {
					import: false
				},
				src: ['src/css/*.css']
			}
		},
		// Run the watcher (Useful to run while coding to pick up on errors.)
		watch: {
		    html: {
		        files: ['<%= htmlhint.build.src %>'],
		        tasks: ['htmlhint']
		    },
		    js: {
		        files: ['<%= jshint.files %>'],
		        tasks: ['jshint']
		    },
		    less: {
                files: ['src/less/*.less'],
                tasks: ['src/less', '<%= csslint.strict %>'],
                options: {
                    nospawn: true
                }
            }
		},
		'ftp-deploy': {
			// Deploy to Production
			build: {
				auth: {
					host: 'www.auroradigital.net',
					port: 21,
					authKey: 'key2',
					authPath: 'ftppass.json'
				},
				src: './',
				dest: '/auroradigital.net/wwwroot',
				exclusions: [
					'.git',
					'_Network Sites',
					'node_modules',
					'src',
					'.gitignore',
					'ftppass.json',
					'Gruntfile.js',
					'package.json'
				]
			},
			// Deploy to Development
			builddev: {
				auth: {
					host: 'dev.auroradigital.net',
					port: 21,
					authKey: 'key1',
					authPath: 'ftppass.json'
				},
				src: './',
				dest: '/ads',
				exclusions: [
					'.git',
					'_Network Sites',
					'node_modules',
					'src',
					'.gitignore',
					'ftppass.json',
					'Gruntfile.js',
					'package.json'
				]
			}
		}
    });
	
	grunt.loadNpmTasks('grunt-contrib-csslint');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-jasmine');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-concat-css');

	grunt.registerTask('deploy-prod', ['ftp-deploy:build']);
	grunt.registerTask('deploy-dev', ['ftp-deploy:builddev']);
    grunt.registerTask('default', ['less', 'concat', 'concat_css', 'uglify', 'cssmin']);
};