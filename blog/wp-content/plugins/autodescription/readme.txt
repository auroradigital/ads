=== The SEO Framework ===
Contributors: Cybr
Donate link: https://theseoframework.com/
Tags: open graph, description, automatic, generate, generator, title, breadcrumbs, ogtype, meta, metadata, search, engine, optimization, seo, framework, canonical, redirect, bbpress, twitter, facebook, google, bing, yahoo, jetpack, genesis, woocommerce, multisite, robots, icon, cpt, custom, post, types, pages, taxonomy, tag, sitemap, sitemaps, screenreader, rtl
Requires at least: 3.6.0
Tested up to: 4.5.0
Stable tag: 2.5.1
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

The SEO Framework makes sure your Website's SEO is always up-to-date without any configuration needed. It also has support for extending.

== Description ==

= The SEO Framework =

**The lightning fast all in one automated SEO optimization plugin for WordPress**

> <strong>This plugin strongly helps you create better SEO value for your content.</strong><br />
> But at the end of the day, it all depends on how entertaining or well-constructed your content or product is.
>
> No SEO plugin does the magic thing to be found instantly. But doing it right helps a lot.<br />
> The SEO Framework helps you doing it right. Give it a try!
>
> The Default Settings are recommended in the SEO Settings page. If you know what you're doing, go ahead and change them! Each option is also documented.

= What this plugin does, in a few lines =
* Automatically configures SEO for every page, post, taxonomy and term.
* Allows you to adjust the SEO globally.
* Allows you to adjust the SEO for every applicable page.
* Shows you how to improve your SEO with a beautiful SEO bar for each supported Post, Page and Taxonomy.
* Helps your pages get ranked distinctively through various Metatag and scripting techniques.
* Helps your pages get shared more beautiful through Facebook, Twitter and other social sites.
* Allows plugin authors to easily extend this plugin.
* Supports custom post types, like WooCommerce and bbPress.
* Automatically upgrades itself from Genesis SEO.
* Allows for easy SEO plugin switch using a tool.

*Read **Transferring SEO Content using SEO Data Transporter** below for SEO plugin transitioning instructions.*

= Numbers don't lie =
Optimizing SEO is a fundamental process for any website. So we try to be non-intrusive with The SEO Framework.
The SEO Framework is byte and process optimized on PHP level, with each update the optimization is improved when possible.

* This plugin is written with massive and busy (multi-)sites in mind.
* This plugin is 197% to 867% faster compared to other popular SEO plugins.
* This plugin consumes 177% to 260% fewer server resources than other popular SEO plugins.
* 15% fewer database interactions (numbers may vary on this one depending on plugin compatibility).
* 100% fewer advertisements. Let's keep it that way.

= Completely pluggable =
The SEO Framework also features pluggable functions. All functions are active and can be called within the WordPress Loop.
This allows other developers to extend the plugin wherever needed.
We have also provided an API documentation located at [The SEO Framework API Docs](http://theseoframework.com/docs/api/).

= Still not convinced? Let's dive deeper =

**This plugin automatically generates:**

* Description, with anti-spam techniques
* Title, with super-fast 'wrong themes' support (so no buffer rewriting!)
* Various Open Graph tags
* Special Open Graph description, which organically integrates with the Facebook and Twitter snippets.
* Extended Open Graph Images support, including image manipulation.
* Canonical, with full WPMUdev Domain Mapping and HTTPS support
* LD+Json script that adds extended search support for Google Search and Chrome
* LD+Json script for Knowledge Graph (Personal/Business site relations, name and logo)
* LD+Json Breadcrumbs script
* Publishing and editing dates
* Link relationships, with full WPMUdev Domain Mapping and HTTPS support
* Various Facebook and Twitter Meta tags
* Sitemap

**This plugin allows you to manually set these values for each post, page and taxonomy:**

* Title
* Description
* Canonical URL
* Robots (nofollow, noindex, noarchive)
* Redirect, with MultiSite spam filter
* Local on-site search settings

**This plugin allows you to adjust various site settings:**

* Title and Description Separators
* Title Additions Location
* Auto Description Output
* Robots for Archives
* Robots for the whole site
* Home Page Description, Title, Tagline and various other options
* Facebook Social integration
* Twitter Social integration
* Open Graph Meta output
* Shortlink tag output
* Post publishing time output
* Link relationships
* Google/Bing Webmaster verification
* Google Knowledge Graph
* Sitemap
* Robots.txt
* And much more

**This plugin helps you to create better content, at a glance. By showing you:**

* If the title is too long, too short and/or automatically generated.
* If the description is too long, too short and/or automatically generated.
* If the description uses some words too often.
* If the page is indexed, redirected, followed and/or archived.
* If your website is publicly accessible.

**We call this The SEO Bar. Check out the [Screenshots](https://wordpress.org/plugins/autodescription/screenshots/#plugin-info) to see how it helps you!**

> This plugin is fully compatible with the [Domain Mapping plugin by WPMUdev](https://premium.wpmudev.org/project/domain-mapping/) and the [Domain Mapping plugin by Donncha](https://wordpress.org/plugins/wordpress-mu-domain-mapping/).<br />
> This plugin is now also compatible with all kinds of custom post types.<br />
> This will **prevent canonical errors**. This way your site will always be correctly indexed, no matter what you use!<br />
>
> This plugin is also completely ad-free and has a WordPress integrated clean layout. As per WordPress.org plugin guidelines and standards.

= Caching =

This plugin's code is highly optimized on PHP-level and uses variable, object and transient caching. This means that there's little extra page load time from this plugin, even with more Meta tags used.
A caching plugin isn't even needed for this plugin as you won't notice a difference, however it's supported wherever best suited.

**If you use object caching:**
The output will be stored for each page, if you've edited a page the page output Meta will stay the same until the object cache expires. So be sure to clear your object cache or wait until it expires.

= Compatibility =

**Basics:**

* Full internationalization support through WordPress.org.
* Extended Multibyte support (CJK).
* Full Right to Left (RTL) support.
* Color vision deficiency accessibility.
* Screen-reader accessibility.
* Admin screen: Posts, Pages, Taxonomies, Terms, Custom Post Types.
* Front-end: Every page, post, taxonomy, term, custom post type, search request, 404, etc.

**Plugins:**

* W3 Total Cache, WP Super Cache, Batcache, etc.
* WooCommerce: Shop Page, Products, Product Galleries, Product Categories and Product Tags.
* Custom Post Types, (all kinds of plugins) with automatic integration.
* WPMUdev and Donncha's Domain Mapping with full HTTPS support.
* WPMUdev Avatars for og:image and twitter:image if no other image is found.
* bbPress: Forums, Topics, Replies.
* BuddyPress profiles.
* AnsPress Questions and Pages, also Canonical errors have been fixed.
* StudioPress SEO Data Transporter for Posts, Pages, Taxonomies and Terms.
* WPML, URL's, sitemap and per-page/post SEO settings. (The full and automated compatibility is being discussed with WPML.)
* qTranslate X, URL's, limited sitemap and per-page/post SEO settings (through shortcodes by set by qTranslate X).
* Jetpack modules: Custom Content Types (Testimonials, Portfolio), Infinite Scroll, Photon.
* Most popular SEO plugins, let's not get in each other's way.
* Many, many other plugins, yet to confirm.

**Themes:**

* All themes.
* Special extended support for Genesis & Genesis SEO. This plugin takes all Post, Page, Category and Tag SEO values from Genesis and uses them within The SEO Framework Options. The easiest upgrade!

**Caches:**

* Opcode (optimized)
* Staticvar
* Object for database calls
* Transients for process intensive operations
* CDN for Open Graph and Twitter images

If you have other popular SEO plugins activated, this plugin will automatically prevent SEO mistakes by deactivating itself on almost every part.
It will however output robots metadata, LD+Json and og:image, among various other meta data which are bound to social media.

= Transferring SEO data using SEO Data Transporter =

Because this plugin was initially written to extend the Genesis SEO, it uses the same option name values. This makes transferring from Genesis SEO to The SEO Framework work automatically.

> If you didn't use Genesis SEO previously, Nathan Rice (StudioPress) has created an awesome plugin for your needs to transfer your SEO data.
>
> Get the [SEO Data Transporter from WordPress.org](https://wordpress.org/plugins/seo-data-transporter/).
>
> Usage:<br />
> 1. Install and activate SEO Data Transporter.<br />
> 2. Go to the <strong>SEO Data Transporter menu within Tools</strong>.<br />
> 3. Select your <strong>previous SEO plugin</strong> within the first dropdown menu.<br />
> 4. Select <strong>Genesis</strong> within the second dropdown menu.<br />
> 5. Click <strong>Analyze</strong> for extra information about the data transport.<br />
> 6. Click <strong>Convert</strong> to convert the data.
>
> The SEO Framework now uses the same data from the new Genesis SEO settings on Posts, Pages and Taxonomies.

= Other notes =

*Genesis SEO will be disabled upon activating this plugin. This plugin takes over and extends Genesis SEO.*

***The Automatic Description Generation will work with any installation, but it will exclude shortcodes. This means that if you use shortcodes or a page builder, be sure to enter your custom description or the description will fall short.***

***The home page tagline settings won't have any effect on the title output if your theme's title output is not written according to the WordPress standards, which luckily are enforced strongly on new WordPress.org themes since recently.***

> <strong>Check out the "[Other Notes](https://wordpress.org/plugins/autodescription/other_notes/#Other-Notes)" tab for the API documentation.</strong>

*I'm aware that radio buttons lose their input when you drag the metaboxes around. This issue is fixed since WordPress 4.5.0 (alpha and later).*
*But not to worry: Your previous value will be returned on save. So it's like nothing happened.*

== Installation ==

1. Install The SEO Framework either via the WordPress.org plugin directory, or by uploading the files to your server.
1. Either Network Activate this plugin or activate it on a single site.
1. That's it!

1. Let the plugin automatically work or fine-tune each page with the metaboxes beneath the content or on the taxonomy pages.
1. Adjust the SEO settings through the SEO settings page if desired. Red checkboxes are rather left unchecked. Green checkboxes are default enabled.

== Screenshots ==

1. This plugin shows you what you can improve, at a glance. With full color vision deficiency support.
2. Hover over any of the SEO Bar's items to see how you can improve the page's SEO. Red is bad, orange is okay, green is good. Blue is situational.
3. The dynamic Post/Page SEO settings Metabox. This box is also neatly implemented in Categories and Tags.
4. The SEO Settings Page. With over 70 settings, you are in full control. Using the Default Settings and filling in the Knowledge Graph Settings is recommended to do.

== Frequently Asked Questions ==

= Is The SEO Framework Free? =

Absolutely! It will stay free as well, without ads or nags!

= I have a feature request, I've found a bug, a plugin is incompatible... =

Please visit [the support forums](https://wordpress.org/support/plugin/autodescription) and kindly tell me about it. I try to get back to you within 48 hours. :)

= I am a developer, how can I help? =

The SEO Framework is currently a one-man project. However, any input is greatly appreciated and everything will be considered.
Please leave feature requests in the Support Forums and I will talk you through the process of implementing it if necessary.

= I'm not a developer, how can I help? =

A way of donating is available through the donation link on the plugin website.
However, you can also greatly help by telling your friends about this plugin :).

= I want to remove or change stuff, but I can't find an option! =

The SEO Framework is very pluggable on many fields. Please refer to the [Other Notes](https://wordpress.org/plugins/autodescription/other_notes/).
Please note that a free plugin is underway which will allow you to change all filters from the dashboard. No ETA yet.

= No ads! Why? =

Nope, no ads! No nags! No links! Never!
Why? Because I hate them, probably more than you do. :(

I also don't want to taint your website from the inside, like many popular plugins do.
Read more about this on the [Plugin Guidelines, Section 7](https://wordpress.org/plugins/about/guidelines/).

***This plugin does leave one link to theseoframework.com in the plugin activation page. But that's OK, right? <3***

Advertisements are made to control your behavior, slowly and certainly you'll be persuaded by them, even if you don't like the product behind it. Ew! D:

***But how do you make a living?***

Currently, The SEO Framework is non-profit.
This plugin was first released to the public in March 15th, 2015. From there it has grown, from 179 lines of code, to more than 17100 lines.
With over 640,000 characters of code written, this plugin is absolutely a piece of art in my mind.
And that's what it should stay, (functional) art.
I trust that people are good at heart and will tell their friends and family about the things they enjoy the most, what they're excited about, what they find beneficial or even beautiful.

With The SEO Framework I try to achieve exactly that. It's made with <3.

= Does this plugin collect my data? =

Absolutely not! Read more about this on the [Plugin Guidelines, Section 7](https://wordpress.org/plugins/about/guidelines/).

= Premium version? =

Nope! Only premium extensions. These are being developed.

= If a premium extensions is released, what will happen to this plugin? =

This plugin is built to be an all-in-one SEO solution, so:

No advertisements about the premium extensions will be placed within this plugin.
No features will be removed or replaced for premium-only features.
The premium extensions will most likely only be used for big-business SEO. Which are very difficult to develop and which will confuse most users anyway.

= I've heard about an extension manager, what's that? =

Currently it's not available. When it is, it will allow you to download and activate extensions for The SEO Framework. It will support both multisite and single-site and the registration will be based on the Akismet plugin.

= I've heard that X SEO plugin does Y, why doesn't The SEO Framework do Y? =

I have no idea what you're talking about! What's X and what's Y? Silly :3.
Please be more elaborate in [the support forums](https://wordpress.org/support/plugin/autodescription), let's get Y working in The SEO Framework (if reasonable)!

= The sitemap doesn't contain categories, is this OK? =

This is not a problem and is currently done so by design to safe processing power. Search Engines love crawling WordPress because its structure is consistent and well known.
The lack of categories in the sitemap is currently extensively covered through a breadcrumb script output on every post which has a category. This way Search Engines will know of the existence of the category and will index it.

= What's does the application/ld+json script do? =

The LD+Json scripts are Search Engine helpers which tell Search Engines how to connect and index the site. They tell the Search Engine if your site contains an internal search engine, what sites you're socially connected to and what page structure you're using.

= My home page title is different from the og:title, or doesn't do what I told it to. =

The theme you're using is using outdated standards and is therefore doing it wrong. Inform your theme author about this issue.

**This is very, very bad for your website's SEO value.**

Give the theme author this link: https://codex.wordpress.org/Title_Tag
And this link: https://make.wordpress.org/themes/2015/08/25/title-tag-support-now-required/

And give the theme author these pieces of code:
`//* functions.php
add_theme_support( 'title-tag' );`

`//* header.php
if ( ! function_exists( '_wp_render_title_tag' ) ) {
	//* Below WordPress 4.1 compatibility
	function theme_slug_render_title() {
		/**
		 * Please, don't add any parameters to the wp_title function, except the empty one provided. Don't 'beautify' the title: It's bad for SEO.
		 * Don't add anything else between the title tag. No blogname, nothing.
		 */
		?><title><?php wp_title(''); ?></title><?php
	}
	add_action( 'wp_head', 'theme_slug_render_title' );
}`
If you know your way around PHP, you can speed up this process by replacing the `wp_title()` function with `wp_title('')` within `header.php`.

= The meta data is not being updated, and I'm using a caching plugin. =

All The SEO Framework's metadata is put into Object cache when a caching plugin is available. The descriptions are put into Transients. Please be sure to clear your cache.
If you're using W3 Total Cache you might be interested in [this free plugin](https://wordpress.org/plugins/w3tc-auto-pilot/) to do it for you.

= Ugh, I don't want anyone to know I'm using The SEO Framework! =

Aww :(
Oh well, here's the filter you need to remove the HTML tags saying where the Meta tags are coming from:
`add_filter( 'the_seo_framework_indicator', '__return_false' );`

= I'm fine with The SEO Framework, but not with you! =

Well then! D: We got off on the wrong foot, I guess..
If you wish to remove only my name from your HTML code, here's the filter:
`add_filter( 'sybre_waaijer_<3', '__return_false' );`

= I want to transport SEO data from other plugins to The SEO Framework, how do I do this? =

Please refer to this small guide: [SEO Data Migration](http://theseoframework.com/docs/seo-data-migration/).

Transporting Terms and Taxonomies data currently isn't supported.

== Changelog ==

= 2.5.1 - Undocumented Properties =

**Summarized:**

* This update addresses issues with the Facebook protocol caused by overlooked (and undocumented) terms. This makes sure the Facebook meta tags work correctly and as per standards.
* Also, all Posts Page data has been fixed and is now being fetched correctly.

**SEO Tip of the Update:**

* Having a faster website will improve your Search Engine Results Page (SERP) ranking.
* It will also prevent potential visitors from hitting the back button if they can't reach your website in a timely manner.
* See SEO Tip of The Update (2.3.9) for related information on how page speed affects SEO. You can find all the previous SEO tips within the plugin folder.

**For everyone:**

* Added: The 'blog' og:type type for the blog page or homepage when it's a Post Page.
* Changed: Page type has been set to `website` rather than `article` when no og:image has been provided on a post to adhere to the standard, this will still output an error in the validator although it's correct.
* Improved: Standardized the default permalink structure URL and reduced memory usage.
* Improved: Shortlink generation time.
* Fixed: The Blog Page now listens to the robots settings.
* Fixed: The Blog Page now listens the custom Title set.
* Fixed: The Blog Page auto-description InPost Metabox placeholder is now what it outputs on the front-end.
* Fixed: Usage of "name" instead of "property" in OG/Facebook meta tags. Sometimes Facebook fixed this automatically, see "List of property fixes" below for more information.

**For developers:**

* Added: New filter.
* Added: Bumped year of copyright.
* Changed: Space after each exclamation mark to maintain flow in PHP from being mixed.
* Cleaned up code.

**List of property fixes:**

* article:author
* article:publisher
* fb:app_id
* article:published_time
* article:modified_time

= 2.5.0 - Vibrant SEO =

**Summarized:**

* This is a massive update which makes The SEO Framework much easier to understand, with colorized counters, color vision deficiency support and many more changes.
* The SEO Framework now also has better support for robots to access and understand your website.
* One way robots better understands your website is that Social (Auto-)Descriptions have received an overhaul since they abide to different standards, they may also be much longer!
* The Robots.txt file output has been reworked and will now prevent notifications in Google Webmaster Tools.
* WooCommerce has gained extra support with multiple Social Open Graph Product Images and front-end AnsPress question descriptions now reflect the admin side.
* Accessibility and understanding of how this plugin handles titles has been improved when adding custom titles.
* Theme load has been reduced with removal of unneeded filters as The SEO Framework overrides them.
* Many requests by The SEO Framework users have been instated, most of them are hidden in filters.
* For developers, a way of adding custom page ID's is now possible through a filter. This makes sure this plugin is extensible and compatible on a new big layer.
* A few functions have been rewritten, and because of this and other big changes for developers and super-users alike, a version bump has been put in place.
* And many fixes have been put in place. These fixes will make sure everything works as expected in special cases.

**SEO Tip of the Update:**

* Images are a great way to attract visitors. Not only will it make your site livelier, visitors will also know instantly what the subject is about or related to.
* Images are also required for Posts to adhere to the article standard set by Google for generating better Rich Snippets to be shown in the Search Engine Results Page (SERP).
* Force yourself and your team to improve the SEO of your site by using the [Require Featured Image](https://wordpress.org/plugins/require-featured-image/) plugin by David Hayes.

**For everyone:**

* Added: All WooCommerce Product Images from the gallery are now also added with multiple `og:image` meta tags (with dupe detection and automatic resizing if needed). Now users can now scroll through and select your item images before sharing on social sites (when supported by the Social Site)!
* Added: Colorized character counters! These subtle colors now let you know you're Doing it Right instantly without going back to the SEO Bar.
* Added: Custom Title now shows the possible additions to deliver a more expected experience. This also takes the Custom HomePage tagline into account. This only works on the following conditions:
  * HomePage Title: If Document Title Additions Location is set to "Left". When using RTL languages, it should be "Right".
  * All other Titles: If Document Title Additions Location is set to "Right". When using RTL languages, it should be "Left".
* Added: Removed line breaks from manual descriptions on save.
* Added: Static caching for the current page in LD+json breadcrumbs to improve generation time.
* Added: The Custom Home Page Title placeholder now reflects changes made throughout the settings Metabox.
* Changed: Global Twitter and Facebook tags output are now enabled by default, this change will only affect new installations or on settings reset.
* Changed: Default Title location settings are switched when an RTL language is active, this change will only affect new installations or on settings reset.
* Changed: Robots.txt now allows for query args by default instead of blocking them.
* Changed: Disabled OG Meta tags output when Add Meta Tags is active.
* Changed: Color changes to the SEO Bar to fully support all vision deficiency spectra. This also makes the SEO bar more vibrant and easier on your eyes.
* Improved: Slightly improved JavaScript speed by caching more variables.
* Improved: The Breadcrumb home URL now also considers the Domain Mapping domains instead of only the current domain.
* Improved: Multiple consecutive empty paragraph entries no longer generate equivalent spaces in the automated description.
* Improved: The Home Page Slogan now alters the Custom Home Page Title dynamically.
* Improved: OG and Twitter Auto Generated description Meta tags now don't contain the Title and Blogname for a more organic social experience.
* Improved: OG and Twitter Auto Generated description meta tags are now up to 200 characters long.
* Improved: Hover messages from the SEO Bar are now aligned to the left so they're easier to read. When using RTL languages, it's aligned ot the right.
* Improved: Hover messages from the SEO Bar now contain an extra line break when duplicated words are highlighted.
* Improved: The hover message balloon arrow now can't overflow over the balloon's corners.
* Improved: Breadcrumb generation time.
* Improved: Cache key generation time.
* Improved: Overal plugin speed, again :).
* Fixed: SEO Column bar was registered for load on Post and Page edit screens, even if it wasn't visible.
* Fixed: WooCommerce admin page product overview layout was messed up on smaller (tablet to 15") screens by the addition of The SEO Bar. It now all mostly fits. Further optimization is left in the hands of WooCommerce.
* Fixed: Bug with AnsPress where not all page descriptions are fetched correctly on the front-end. Because of the bugfix, the description cache will be flushed upon update. The cache will clean itself up within 7 days automatically.
* Fixed: WooCommerce main shop page took the title of the latest product.
* Fixed: Inconsistent cache key for WooCommerce shop page.
* Fixed: Robots.txt sitemap wasn't pointed to correctly on Subdirectory MultiSite installations.
* Fixed: Robots.txt sitemap wasn't pointed to correctly with mapped domains on MultiSite sites.
* Fixed: Bug for Home Title in the breadcrumbs when the homepage is a page. Because of the bugfix, the LD+Json cache will be flushed upon update. The cache will clean itself up within 7 days automatically.
* Fixed: Cache naming bug in taxonomy and terms admin pages displaying the front page description.
* Removed: /wp-includes/ blocking through robots.txt, this was accidentally added a long time ago and the removal should resolve issues with Google Webmaster Tools.
* Removed: Dutch Translation Files as they're maintained now through WordPress.org.

**For developers:**

* Added: New function `AutoDescription_Render::the_home_url_from_cache( $force_slash )`.
* Added: New function `AutoDescription_Generate::parse_og_image( $image_id, $args )`, parses image to a maximum of 1500px width or height based on biggest factor and saves it if not yet parsed, returns the URL of the parsed image.
* Added: New function `AutoDescription_Render::get_the_real_ID()`. Fetches the real ID for all pages, posts, terms, taxonomies and CPT, including WooCommerce and AnsPress.
* Added: New function `AutoDescription_Sanitize::s_description()`. Removes spaces and breaks from the description and created a clean flowing single line. Sanitizes as well.
* Added: New filters.
* Added: Generation time output on the URL and Description when debugging is activated.
* Added: Removal of all `pre_get_document_title` filters prior to execution to reduce theme load. This is done at `init` priority `1`.
* Changed: `pre_get_document_title` filter priority is now `10`, was `99` for easier manipulation. This is run at `init` priority `1`.
* Changed: Rewritten `AutoDescription_Generate::generate_description_from_id()` as it was previously built upon bugfixes and chance.
* Changed: Default WordPress robots.txt is now completely overwritten to maintain compatibility.
* Changed: `AutoDescription_Generate::generate_description()` function parameters have now been put into an arguments array. This array is passed onto the following functions as well: `AutoDescription_Generate::description_from_custom_field()` and `AutoDescription_Generate::generate_description_from_id()`
* Changed: `the_seo_framework_robots_allow_queries` filter became `the_seo_framework_robots_disallow_queries`.
* Changed: Default and warned settings are now initialized at `after_theme_setup, 0` rather than `plugins_loaded, 5` to support RTL early.
* Improved: Default arguments for the Title, URL and Image are now auto-correcting when called to incorrectly in some scenarios. Keep an eye out on the error log if you're using the parsers directly.
* Improved: Debug constants have been pushed in public class variables, effectively reducing plugin run time.
* Fixed: `AutoDescription_Generate::get_separator()` didn't listen to the escape parameter because of the cache. This function has been reworked for better caching.
* Removed: `$args['post_id']` argument from the title functions as it was unused. Use `$args['term_id']` instead.
* Removed: 2nd argument ($tt_id) from `AutoDescription_Generate::generate_excerpt()`. Use the first argument ($post_id) instead, this induces a parameter switch.
* Cleaned up code.

= 2.4.3.1 - Naming Things =

**Summarized:**

* Small update which fixes two bugs reported by my dear users, Thanks! :)
* Search Pages are now also generating a new cache key so they can't conflict with the home page cache anymore in special scenarios.

**For everyone:**

* Improved: Massively improved plugin speed on 404 and search pages by eliminating redundant calls.
* Improved: Cache generation key is now using static cache correctly.
* Fixed: The object cache key is now generating a different cache key for search queries before checking if it's the home page. This will prevent Search Queries to take over the home page cache when using a premium theme to design a one-pager front-page through widgets.
* Fixed: Sitemap and robots example links weren't correct when the permalink structure doesn't end with a slash.
* Fixed: Fatal error caused by typo on search pages.

**For developers:**

* Added: Extra parameter to `AutoDescription_Generate::the_url()` for forcing trailing slashes.

= Full changelog =

**The full changelog can be found [here](http://theseoframework.com/?cat=3).**

== Other Notes ==

= Filters =

= Add any of the filters to your theme's functions.php or a plugin to change this plugin's output. =

Learn about them here: [The SEO Framework filters](http://theseoframework.com/docs/api/filters/)

= Constants =

= Overwrite any of these constants in your theme's functions.php or a plugin to change this plugin's output by simply defining the constants. =

View them here: [The SEO Framework constants](http://theseoframework.com/docs/api/constants/)

= Actions =

= Use any of these actions to add your own output. =

They are found here: [The SEO Framework actions](http://theseoframework.com/docs/api/actions/)

= Settings API =

= Add settings to and interact with The SEO Framework. =

Read how to here: [The SEO Framework Settings API](http://theseoframework.com/docs/api/settings/)

= Beta Version =

= Stay updated with the latest version before it's released? =

The beta is available [on Github](https://github.com/sybrew/the-seo-framework). Please note that changes might not reflect the final outcome of the full version release.
